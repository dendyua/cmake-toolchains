
function(_qnx_toolchain_init)
	# default target arch
	set(_qnx_arch "armle-v7")

	# env vars
	set(_env_vars
		QNX_HOST
		QNX_TARGET
		QNX_CONFIGURATION
		QNXLM_LICENSE_FILE
	)

	set(_env_var_need_path_conversion_QNX_HOST YES)
	set(_env_var_need_path_conversion_QNX_TARGET YES)
	set(_env_var_need_path_conversion_QNX_CONFIGURATION NO)
	set(_env_var_need_path_conversion_QNXLM_LICENSE_FILE NO)

	set(_env_var_is_mandatory_QNX_HOST YES)
	set(_env_var_is_mandatory_QNX_TARGET YES)
	set(_env_var_is_mandatory_QNX_CONFIGURATION NO)
	set(_env_var_is_mandatory_QNXLM_LICENSE_FILE NO)

	foreach ( _env_var ${_env_vars} )
		set(_cache_var QNX_ENV_${_env_var})

		if ( "${${_cache_var}}" STREQUAL "" )
			set(${_cache_var} "$ENV{${_env_var}}")
			if ( NOT "${${_cache_var}}" STREQUAL "" )
				if ( _env_var_need_path_conversion_${_env_var} )
					string(REPLACE "\\" "/" ${_cache_var} "${${_cache_var}}")
				endif()
				set(${_cache_var} "${${_cache_var}}" CACHE INTERNAL "" FORCE)
				mark_as_advanced(${_cache_var})
			else()
				if ( _env_var_is_mandatory_${_env_var} )
					message(FATAL_ERROR "Specify ${_env_var}")
				endif()
			endif()
		endif()

		if ( NOT "${${_cache_var}}" STREQUAL "" )
			set(ENV{${_env_var}} "${${_cache_var}}")
		endif()
	endforeach()

	set(_qnx_tool_prefix "${QNX_ENV_QNX_HOST}/usr/bin/ntoarmv7")


	# detect host platform
	get_filename_component(_qnx_host_arch "${QNX_ENV_QNX_HOST}" NAME)
	get_filename_component(_qnx_host_os_path "${QNX_ENV_QNX_HOST}" DIRECTORY)
	get_filename_component(_qnx_host_os "${_qnx_host_os_path}" NAME)
	unset(_qnx_host_os_path)
	set(_qnx_host_platform "${_qnx_host_os}/${_qnx_host_arch}")

	set(QNX_HOST_PLATFORM "${_qnx_host_platform}" CACHE INTERNAL "" FORCE)
	set(QNX_ARCH "${_qnx_arch}" CACHE INTERNAL "" FORCE)


	# create gcc wrapper
	set(_qnx_tool_wrapper_dir "${CMAKE_BINARY_DIR}/qnx_tool_wrappers")
	execute_process(COMMAND "${CMAKE_COMMAND}" -E make_directory "${_qnx_tool_wrapper_dir}")

	if ( WIN32 AND "${CMAKE_GENERATOR}" STREQUAL "MinGW Makefiles" )
		set(_qnx_use_win32 YES)
	else()
		set(_qnx_use_win32 NO)
	endif()

	if ( _qnx_use_win32 )
		set(_qnx_tool_suffix ".exe")
		set(_qnx_tool_wrapper_suffix ".bat")
	else()
		set(_qnx_tool_suffix "")
		set(_qnx_tool_wrapper_suffix ".sh")
	endif()

	function (_qnx_create_tool_wrapper TOOL TOOL_PATH OPTIONS)
		string(TOLOWER "${TOOL}" _qnx_tool_lower)
		set(_qnx_tool "${TOOL_PATH}${_qnx_tool_suffix}")
		if ( NOT "${OPTIONS}" STREQUAL "" )
			set(_qnx_tool "${_qnx_tool} ${OPTIONS}")
		endif()
		set(_qnx_tool_wrapper_prefix "${_qnx_tool_wrapper_dir}/${_qnx_tool_lower}")
		set(_qnx_tool_wrapper "${_qnx_tool_wrapper_prefix}${_qnx_tool_wrapper_suffix}")

		if ( _qnx_use_win32 )
			set(_env_string)
			foreach ( _env_var ${_env_vars} )
				set(_cache_var QNX_ENV_${_env_var})
				if ( NOT "${${_cache_var}}" STREQUAL "" OR _env_var_is_mandatory_${_env_var} )
					if ( _env_var_need_path_conversion_${_env_var} )
						string(REPLACE "/" "\\" _native_path "${${_cache_var}}")
					else()
						set(_native_path "${${_cache_var}}")
					endif()
					set(_env_string "${_env_string}set ${_env_var}=${_native_path}\n")
				endif()
			endforeach()

			string(REPLACE "/" "\\" _qnx_tool_native "${_qnx_tool}")

			file(WRITE "${_qnx_tool_wrapper}"
				"echo args=%*\n"
				"${_env_string}"
				"${_qnx_tool_native} %*\n"
			)
		else()
			set(_env_string)
			foreach ( _env_var ${_env_vars} )
				set(_cache_var QNX_ENV_${_env_var})
				if ( NOT "${${_cache_var}}" STREQUAL "" OR _env_var_is_mandatory_${_env_var} )
					set(_env_string "${_env_string}export ${_env_var}=\"${${_cache_var}}\"\n")
				endif()
			endforeach()

			file(WRITE "${_qnx_tool_wrapper}"
				"#!/bin/sh\n"
				"${_env_string}"
				"${_qnx_tool} \"$@\"\n"
			)

			if ( CMAKE_HOST_UNIX )
				execute_process(COMMAND chmod +x "${_qnx_tool_wrapper}")
			endif()
		endif()

		set(CMAKE_${TOOL} "${_qnx_tool_wrapper}" CACHE FILEPATH "" FORCE)
	endfunction()

	if ( _qnx_use_qcc )
		_qnx_create_tool_wrapper(C_COMPILER   "${QNX_ENV_QNX_HOST}/usr/bin/qcc" "-Vgcc_ntoarmv7le")
		_qnx_create_tool_wrapper(CXX_COMPILER "${QNX_ENV_QNX_HOST}/usr/bin/qcc" "-Vgcc_ntoarmv7le -lang-c++")
	else()
		_qnx_create_tool_wrapper(C_COMPILER   "${_qnx_tool_prefix}-gcc" "")
		_qnx_create_tool_wrapper(CXX_COMPILER "${_qnx_tool_prefix}-g++" "")
	endif()

	set(CMAKE_AR     "${_qnx_tool_prefix}-ar"     CACHE FILEPATH "" FORCE)
	set(CMAKE_RANLIB "${_qnx_tool_prefix}-ranlib" CACHE FILEPATH "" FORCE)

	function (_qnx_force_linker_cpp_flags)
		if ( _qnx_use_qcc )
			set(_cpp_flag "-lcpp")
		else()
			set(_cpp_flag "-lstdc++")
		endif()

		foreach ( _qnx_type ${ARGN} )
			set(_qnx_var CMAKE_${_qnx_type}_LINKER_FLAGS)
			string(FIND "${${_qnx_var}}" "${_cpp_flag}" _qnx_index)
			if ( ${_qnx_index} EQUAL -1 )
				set(${_qnx_var} "${${_qnx_var}} ${_cpp_flag}" CACHE STRING "" FORCE)
			endif()
		endforeach()
	endfunction()

	_qnx_force_linker_cpp_flags(EXE MODULE SHARED)


	foreach ( _qnx_lang C CXX )
		foreach(_qnx_type SHARED_LIBRARY SHARED_MODULE EXE)
			set(CMAKE_${_qnx_type}_LINK_STATIC_${_qnx_lang}_FLAGS "-Wl,-Bstatic" CACHE STRING "" FORCE)
			set(CMAKE_${_qnx_type}_LINK_DYNAMIC_${_qnx_lang}_FLAGS "-Wl,-Bdynamic" CACHE STRING "" FORCE)
		endforeach()

		set(CMAKE_INCLUDE_SYSTEM_FLAG_${_qnx_lang} "-Wp,-isystem," CACHE STRING "" FORCE)

		if ( _qnx_use_qcc )
			set(CMAKE_DEPFILE_FLAGS_${_qnx_lang} "-Wc,-MMD,<DEPFILE> -Wc,-MT,<OBJECT> -Wc,-MF,<DEPFILE>" CACHE STRING "" FORCE)
		else()
			set(CMAKE_DEPFILE_FLAGS_${_qnx_lang} "-MMD -MT <OBJECT> -MF <DEPFILE>" CACHE STRING "" FORCE)
		endif()

		set(CMAKE_${_qnx_lang}_FLAGS "${CMAKE_${_qnx_lang}_FLAGS} -Wno-psabi" PARENT_SCOPE)
	endforeach()
endfunction()


function(qnx_add_extra_sysroot SYSROOT)
	list(APPEND CMAKE_FIND_ROOT_PATH
		"${SYSROOT}/host/${QNX_HOST_PLATFORM}"
		"${SYSROOT}/target/qnx6"
		"${SYSROOT}/target/qnx6/${QNX_ARCH}"
	)
	set(CMAKE_FIND_ROOT_PATH "${CMAKE_FIND_ROOT_PATH}" PARENT_SCOPE)
endfunction()


function(qnx_add_extra_sysroot_target SYSROOT_TARGET)
	list(APPEND CMAKE_FIND_ROOT_PATH
		"${SYSROOT_TARGET}"
		"${SYSROOT_TARGET}/${QNX_ARCH}"
	)
	set(CMAKE_FIND_ROOT_PATH "${CMAKE_FIND_ROOT_PATH}" PARENT_SCOPE)
endfunction()


_qnx_toolchain_init()


set(CMAKE_SYSTEM_NAME "QNX")
set(CMAKE_SYSTEM_VERSION 1)

set(CMAKE_POSITION_INDEPENDENT_CODE YES)

set(_CMAKE_TOOLCHAIN_PREFIX "${QNX_ENV_QNX_HOST}/usr/bin/ntoarmv7-")
set(_CMAKE_TOOLCHAIN_LOCATION "${QNX_ENV_QNX_HOST}/usr/bin")

if ( _qnx_use_qcc )
	set(QNX_USE_LIBCPP YES)
else()
	set(QNX_USE_LIBCPP NO)
endif()

# default sysroot
list(APPEND CMAKE_FIND_ROOT_PATH
	"${QNX_ENV_QNX_HOST}"
	"${QNX_ENV_QNX_TARGET}"
	"${QNX_ENV_QNX_TARGET}/${QNX_ARCH}"
)

# extra root paths
if ( QNX_EXTRA_SYSROOTS )
	set(QNX_EXTRA_SYSROOTS "${QNX_EXTRA_SYSROOTS}" CACHE INTERNAL "" FORCE)
	foreach ( _qnx_extra_sysroot ${QNX_EXTRA_SYSROOTS} )
		qnx_add_extra_sysroot("${_qnx_extra_sysroot}")
	endforeach()
endif()

if ( QNX_EXTRA_SYSROOT_TARGETS )
	set(QNX_EXTRA_SYSROOT_TARGETS "${QNX_EXTRA_SYSROOT_TARGETS}" CACHE INTERNAL "" FORCE)
	foreach ( _qnx_extra_sysroot_target ${QNX_EXTRA_SYSROOT_TARGETS} )
		qnx_add_extra_sysroot_target("${_qnx_extra_sysroot_target}")
	endforeach()
endif()


set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
