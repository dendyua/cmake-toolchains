
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

set(CMAKE_C_COMPILER   /usr/lib/icecc/bin/gcc)
set(CMAKE_CXX_COMPILER /usr/lib/icecc/bin/g++)
