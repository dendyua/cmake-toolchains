
function (__android_setup_toolchain)
	set(var_names)

	function (add_var NAME DEFAULT)
		set(var_names ${var_names} ${NAME} PARENT_SCOPE)
		set(var_default_${NAME} "${DEFAULT}" PARENT_SCOPE)
	endfunction()

	# resolve default NDK directory
	if ("$ENV{ANDROID_HOME}")
		set(default_ndk_dir "$ENV{ANDROID_HOME}/ndk-bundle")
	else()
		set(default_ndk_dir)
	endif()

	add_var(NDK_ROOT "${default_ndk_dir}")
	add_var(TOOLCHAIN "llvm")
	add_var(ARCH "aarch64")
	add_var(API "24")

	foreach (name ${var_names})
		set(env_var "ANDROID_${name}")
		set(cache_var Android_Env_${name})
		set(value ${${cache_var}})

		if ("${value}" STREQUAL "")
			# take value from environment variable
			set(value "$ENV{${env_var}}")

			if ("${value}" STREQUAL "")
				# take value from defaults
				set(value ${var_default_${name}})
			endif()

			if ("${value}" STREQUAL "")
				message(FATAL_ERROR "Specify environment variable: ${env_var}")
			endif()

			set(${cache_var} "${value}" CACHE INTERNAL "" FORCE)
			mark_as_advanced(${cache_var})
		endif()

		message("${cache_var}=${value}")

		set($ENV{${env_var}} "${value}")
	endforeach()

	string(TOLOWER "${CMAKE_HOST_SYSTEM_NAME}" host_system_name)

	set(toolchain_dir "${Android_Env_NDK_ROOT}/toolchains/${Android_Env_TOOLCHAIN}/prebuilt/${host_system_name}-${CMAKE_HOST_SYSTEM_PROCESSOR}")
	set(prefix "${toolchain_dir}/bin/${Android_Env_ARCH}-linux-android-")
	set(compiler_prefix "${toolchain_dir}/bin/${Android_Env_ARCH}-linux-android${Android_Env_API}-")

	# tools
	set(CMAKE_AR      "${prefix}-ar"      PARENT_SCOPE)
	set(CMAKE_LINKER  "${prefix}-ld"      PARENT_SCOPE)
	set(CMAKE_NM      "${prefix}-nm"      PARENT_SCOPE)
	set(CMAKE_OBJCOPY "${prefix}-objcopy" PARENT_SCOPE)
	set(CMAKE_OBJDUMP "${prefix}-objdump" PARENT_SCOPE)
	set(CMAKE_STRIP   "${prefix}-strip"   PARENT_SCOPE)
	set(CMAKE_RANLIB  "${prefix}-ranlib"  PARENT_SCOPE)

	# compilers
	set(CMAKE_C_COMPILER   "${compiler_prefix}clang"   PARENT_SCOPE)
	set(CMAKE_CXX_COMPILER "${compiler_prefix}clang++" PARENT_SCOPE)

	# target system
	set(CMAKE_SYSTEM_NAME Android PARENT_SCOPE)
	set(CMAKE_SYSTEM_VERSION 1  PARENT_SCOPE)

	# where is the target environment
	set(CMAKE_FIND_ROOT_PATH
		"${toolchain_dir}/bin"
		"${toolchain_dir}/sysroot"
		PARENT_SCOPE
	)

	# global includes
	#include_directories("${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/include/c++/${Android_GCC_VERSION}/arm-linux-androideabi")

	# where to look for include, libraries and programs
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH PARENT_SCOPE)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY PARENT_SCOPE)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY PARENT_SCOPE)

	set(CMAKE_SYSROOT "${toolchain_dir}/sysroot" PARENT_SCOPE)
endfunction()


__android_setup_toolchain()



function (__android_noop)
# compiler flags
# It is recommended to use the -mthumb compiler flag to force the generation
# of 16-bit Thumb-1 instructions (the default being 32-bit ARM ones).
set(_compiler_flags "-fPIC -DANDROID -mthumb -Wno-psabi -Werror=return-type --sysroot=${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/sysroot")

if ( "${Android_ARCH}" STREQUAL "armeabi-v7a" )
	set(_compiler_flags "${_compiler_flags} -march=armv7-a -mfloat-abi=softfp")
	if ( Android_NEON )
		set(_compiler_flags "${_compiler_flags} -mfpu=neon")
	endif()
endif()

set(CMAKE_CXX_FLAGS "${_compiler_flags}" CACHE STRING "C++ flags")
set(CMAKE_C_FLAGS   "${_compiler_flags}" CACHE STRING "C flags")

unset( _compiler_flags )


# linker flags
set(_linker_flags "-Wl,--fix-cortex-a8 -lgnustl_shared -lm -llog -landroid --sysroot=${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/sysroot")

set(_no_undefined YES)
if (_no_undefined)
	set(_linker_flags "${_linker_flags} -Wl,--no-undefined")
endif()

set(CMAKE_SHARED_LINKER_FLAGS "${_linker_flags}" CACHE STRING "Linker flags")
set(CMAKE_MODULE_LINKER_FLAGS "${_linker_flags}" CACHE STRING "Linker flags")

unset(_linker_flags)


# set global variable to indicate Andtoid mode
set(ANDROID TRUE)


# add global definitions
add_definitions(-DANDROID)
add_definitions(-D"ANDROID_ARCH=${Android_ARCH}")
endfunction()
