
set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR x86)

find_program(CMAKE_C_COMPILER   i686-w64-mingw32-gcc)
find_program(CMAKE_CXX_COMPILER i686-w64-mingw32-g++)
find_program(CMAKE_AR           i686-w64-mingw32-ar)

if (CMAKE_HOST_SYSTEM_NAME STREQUAL Linux)
    set(CMAKE_FIND_ROOT_PATH /usr/i686-w64-mingw32/sys-root/mingw)
else()
    get_filename_component(find_root_path "${CMAKE_C_COMPILER}/.." ABSOLUTE)
    set(CMAKE_FIND_ROOT_PATH "${find_root_path}" CACHE PATH "")
endif()

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pthread")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pthread")
