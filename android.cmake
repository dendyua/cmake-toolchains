
function ( _android_init_toolchain )

	set(_env_vars
		ANDROID_SDK_ROOT
		ANDROID_TOOLCHAIN_ROOT
	)

	foreach ( _env_var ${_env_vars} )
		set(_cache_var Android_ENV_${_env_var})

		if ( NOT ${_cache_var} )
			set(${_cache_var} "$ENV{${_env_var}}")
			if ( ${_cache_var} )
				set(${_cache_var} "${${_cache_var}}" CACHE INTERNAL "" FORCE)
				mark_as_advanced(${_cache_var})
			else()
				message(FATAL_ERROR "Specify ${_env_var}")
			endif()
		endif()

		if ( ${_cache_var} )
			set(ENV{${_env_var}} "${${_cache_var}}")
		endif()
	endforeach()

	set(_tool_wrapper_dir "${CMAKE_BINARY_DIR}/android_tool_wrappers")
	execute_process(COMMAND "${CMAKE_COMMAND}" -E make_directory "${_tool_wrapper_dir}")

	function ( _android_create_tool_wrapper TOOL TOOL_PATH )
		string(TOLOWER "${TOOL}" _tool_lower)
		set(_tool "${TOOL_PATH}")
		set(_tool_wrapper_prefix "${_tool_wrapper_dir}/${_tool_lower}")
		set(_tool_wrapper "${_tool_wrapper_prefix}")

		set(_env_string)
		foreach ( _env_var ${_env_vars} )
			set(_cache_var Android_ENV_${_env_var})
			set(_env_string "${_env_string}export ${_env_var}=\"${${_cache_var}}\"\n")
		endforeach()

		file(WRITE "${_tool_wrapper}"
			"#!/bin/sh\n"
			"${_env_string}"
			"${_tool} \"$@\"\n"
		)

		execute_process(COMMAND chmod +x "${_tool_wrapper}")

		set(CMAKE_${TOOL} "${_tool_wrapper}" CACHE FILEPATH "" FORCE)
	endfunction()

	set(_tool_prefix "${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/bin/arm-linux-androideabi")

	_android_create_tool_wrapper(C_COMPILER   "${_tool_prefix}-gcc")
	_android_create_tool_wrapper(CXX_COMPILER "${_tool_prefix}-g++")

	set(CMAKE_AR      "${_tool_prefix}-ar"      CACHE PATH "" FORCE)
	set(CMAKE_LINKER  "${_tool_prefix}-ld"      CACHE PATH "" FORCE)
	set(CMAKE_NM      "${_tool_prefix}-nm"      CACHE PATH "" FORCE)
	set(CMAKE_OBJCOPY "${_tool_prefix}-objcopy" CACHE PATH "" FORCE)
	set(CMAKE_OBJDUMP "${_tool_prefix}-objdump" CACHE PATH "" FORCE)
	set(CMAKE_STRIP   "${_tool_prefix}-strip"   CACHE PATH "" FORCE)
	set(CMAKE_RANLIB  "${_tool_prefix}-ranlib"  CACHE PATH "" FORCE)

endfunction()


_android_init_toolchain()


# fixed options
set(Android_GCC_VERSION 4.9)
set(Android_ARCH "armeabi-v7a")
set(Android_NEON YES)


# target system
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)




# where is the target environment
set(CMAKE_FIND_ROOT_PATH
	"${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/bin"
	"${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/sysroot"
	"${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/arm-linux-androideabi"
	"${CMAKE_INSTALL_PREFIX}"
	"${CMAKE_INSTALL_PREFIX}/share"
)


# global includes
include_directories("${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/include/c++/${Android_GCC_VERSION}/arm-linux-androideabi")


# where to look for include, libraries and programs
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)


# compiler flags
# It is recommended to use the -mthumb compiler flag to force the generation
# of 16-bit Thumb-1 instructions (the default being 32-bit ARM ones).
set(_compiler_flags "-fPIC -DANDROID -mthumb -Wno-psabi -Werror=return-type --sysroot=${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/sysroot")

if ( "${Android_ARCH}" STREQUAL "armeabi-v7a" )
	set(_compiler_flags "${_compiler_flags} -march=armv7-a -mfloat-abi=softfp")
	if ( Android_NEON )
		set(_compiler_flags "${_compiler_flags} -mfpu=neon")
	endif()
endif()

set(CMAKE_CXX_FLAGS "${_compiler_flags}" CACHE STRING "C++ flags")
set(CMAKE_C_FLAGS   "${_compiler_flags}" CACHE STRING "C flags")

unset( _compiler_flags )


# linker flags
set(_linker_flags "-Wl,--fix-cortex-a8 -lgnustl_shared -lm -llog -landroid --sysroot=${Android_ENV_ANDROID_TOOLCHAIN_ROOT}/sysroot")

set(_no_undefined YES)
if (_no_undefined)
	set(_linker_flags "${_linker_flags} -Wl,--no-undefined")
endif()

set(CMAKE_SHARED_LINKER_FLAGS "${_linker_flags}" CACHE STRING "Linker flags")
set(CMAKE_MODULE_LINKER_FLAGS "${_linker_flags}" CACHE STRING "Linker flags")

unset(_linker_flags)


# set global variable to indicate Andtoid mode
set(ANDROID TRUE)


# add global definitions
add_definitions(-DANDROID)
add_definitions(-D"ANDROID_ARCH=${Android_ARCH}")
