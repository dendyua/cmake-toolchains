
function (_setup_flags)
	set(_root "${YoctoDir}/toolchains/gcc-linaro-arm-linux-gnueabihf")

	set(_system_include_flag "-isystem${_root}/arm-linux-gnueabihf/include")
	set(_link_dir_flag "-L${_root}/arm-linux-gnueabihf/lib")
	set(_rpath_link_flag "-Wl,-rpath-link,${_root}/arm-linux-gnueabihf/lib")

	set(CMAKE_C_COMPILER   "${_root}/bin/arm-linux-gnueabihf-gcc" CACHE FILEPATH "" FORCE)
	set(CMAKE_CXX_COMPILER "${_root}/bin/arm-linux-gnueabihf-g++" CACHE FILEPATH "" FORCE)
	set(CMAKE_AR           "${_root}/bin/arm-linux-gnueabihf-ar"  CACHE FILEPATH "" FORCE)

	set(_c_cpp_flags
		-march=armv7-a
		-marm -mthumb-interwork
		-mfloat-abi=hard
		-mfpu=neon
		-mtune=cortex-a15
		--sysroot=${TargetSysrootDir}
		${_system_include_flag}
		-fstack-protector
		-O2
		-pipe
		-feliminate-unused-debug-types
	)

	set(_cpp_flags -fpermissive)

	set(_link_flags ${_link_dir_flag} ${_rpath_link_flag} -Wl,--hash-style=gnu)

	string(REPLACE ";" " " _c_flags_string "${_c_cpp_flags}")
	string(REPLACE ";" " " _cpp_flags_string "${_c_cpp_flags};${_cpp_flags}")
	string(REPLACE ";" " " _c_link_flags_string "${_c_cpp_flags};${_link_flags}")
	string(REPLACE ";" " " _cpp_link_flags_string "${_c_cpp_flags};${_cpp_flags};${_link_flags}")

	set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${_c_flags_string}" CACHE STRING "")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_cpp_flags_string}" CACHE STRING "")
	set(CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} -DNDEBUG" CACHE STRING "")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DNDEBUG" CACHE STRING "")
	set(CMAKE_C_LINK_FLAGS   "${CMAKE_C_LINK_FLAGS} ${_c_link_flags_string}" CACHE STRING "")
	set(CMAKE_CXX_LINK_FLAGS "${CMAKE_CXX_LINK_FLAGS} ${_cpp_link_flags_string}" CACHE STRING "")
endfunction()




set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR arm)

_setup_flags()

# only search in the paths provided so cmake doesnt pick
# up libraries and tools from the native build machine
set(CMAKE_FIND_ROOT_PATH "${TargetSysrootDir}" "${HostSysrootDir}")
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Use qt.conf settings
configure_file("${CMAKE_CURRENT_LIST_DIR}/qt.conf.in" "${CMAKE_BINARY_DIR}/qt.conf")
set(ENV{PKG_CONFIG_LIBDIR} "${TargetSysrootDir}/usr/lib/pkgconfig")
set(ENV{QT_CONF_PATH} "${CMAKE_BINARY_DIR}/qt.conf")
set(OE_QMAKE_PATH_EXTERNAL_HOST_BINS "${HostSysrootDir}/usr/bin/qt5" CACHE PATH "" FORCE)

# We need to set the rpath to the correct directory as cmake does not provide any
# directory as rpath by default
set(CMAKE_INSTALL_RPATH)

# Use native cmake modules
set(CMAKE_MODULE_PATH "${TargetSysrootDir}/pas/share/cmake/Modules/")

# add for non /usr/lib libdir, e.g. /usr/lib64
set(CMAKE_LIBRARY_PATH "/pas/lib" "/lib")
